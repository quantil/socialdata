import psycopg2
from config import config


def create_tables():

    """ create tables in the PostgreSQL database"""
    command = (
        """
        CREATE TABLE tweets_data_fech_act_en (
	    id_unique BIGSERIAL PRIMARY KEY,
            id text,
    	    conversation_id text,
            created_at text,
            date text,
            time text,
            timezone text,
            user_id text,
            username text,
            name text,
            place text,
            tweet text,
            mentions text,
            urls text,
            photos text,
            replies_count text,
            retweets_count text,
            likes_count text,
            hashtags text,
            cashtags text,
            link text,
            retweet text,
            quote_url text,
            video text,
            near text,
            geo text,
            source text,
            user_rt_id text,
            user_rt text,
            retweet_id text,
            reply_to text,
            retweet_date text,
            fecha_rastreo DATE NOT NULL DEFAULT CURRENT_DATE);
        """)
    conn = None
    try:
        # read the connection parameters
        params = config()
        # connect to the PostgreSQL server
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        # create table one by one
        ###for command in commands:
        cur.execute(command)
        # close communication with the PostgreSQL database server
        cur.close()
        # commit the changes
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


if __name__ == '__main__':
    create_tables()
