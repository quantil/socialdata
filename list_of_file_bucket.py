# ------------------------------------------------------------------------------------
# Listar los archivos  almacenados en Google Cloud Storage
# Primera lista archivos en español, segunda lista achivos en ingles
# -----------------------------------------------------------------------------------

import datetime
from datetime import timedelta
from random import randint
from google.cloud import storage

"""Programatically interact with a Google Cloud Storage bucket."""

storage_client = storage.Client.from_service_account_json(r'/home/jorgeda/Descargas/Quantil/socialdata-psw.json')
#storage_client = storage.Client.from_service_account_json(r'/home/jorgeda/pruebas_jorge/socialdata-psw.json')
bucket = storage_client.get_bucket('s_d_bucket')

bucketFolder_es = 'tweets_espaniol'
bucketFolder_en = 'tweets_ingles'

def list_files(bucket, bucketFolder):
    """List all files in GCP bucket."""
    files = bucket.list_blobs(prefix=bucketFolder)
    #fileList = [file.name for file in files if '.' in file.name]
    fileList = []
    for i in files:
        #print(i)
        fileList.append(i.name.split('/')[-1])
    fileList = fileList[1:] 
    leng=bucketFolder.split('_')[-1]
    return print('\n','Archivos de Tweets en '+leng+' por fechas de desgarga: ','\n','\n',fileList,'\n')

def list_all_files():
    list_files(bucket, bucketFolder_es)
    list_files(bucket, bucketFolder_en)
    
list_all_files()