# SOCIALDATA >> Descarga masiva de tweets

Este repositorio contiene una herramienta (software) que permite descargar tweets masivamente dependiendo de las reglas que se incluyan.

1. Descarga tweets de diferentes reglas, se utilizaron aproximadamente 120 reglas las cuales estaban divididas en 2 idiomas, cada regla está compuesta por uno o más filtros, esto se mostrará con detalle más adelante.
2. Los tweets al descargarse tienen diferentes métodos de almacenamiento aparte de una API REST que muestra en línea los datos de un dia de descarga. 
3. Los métodos de almacenamiento que tiene disponible son una Base de Datos relacional de código abierto robusta motor **PostgreSQL** y un **Bucket** (unidad de almacenamiento en la nube de Google) de almacenamiento en la nube de **Google Cloud Platform**.

## Tabla de contenido

- [Arquitectura](##arquitectura)
- [Instalación](##instalacion)
- [Accediendo a los datos](##Accediendo a los datos)
- [Licencia](##Licencia) 


## Arquitectura

Esta aplicación utiliza un Módulo llamada **Twint** el cual es una herramienta avanzada de raspado de Twitter escrita en Python, que permite raspar Tweets de perfiles de Twitter sin usar la API de Twitter. 

**Twint** utiliza los operadores de búsqueda de Twitter para permitirte raspar Tweets de usuarios específicos, raspar Tweets relacionados con ciertos temas, hashtags y tendencias. Permitiendo descargar información detallada de los tweets como fecha, retweets, cantidad de retweets, usuarios y nombre de usuario entre otros datos.

La búsqueda se puede parametrizar por medio de ciertas reglas que se almacenen en una  pequeña base de datos en excel, estas reglas pueden ser modificadas o actualizadas dependiendo el caso de uso o la investigación que se desee realizar, cada regla arroja resultados los cuales pueden ser limitados a la hora de la descarga por parámetros previamente definidos, actualmente tiene un límite de 10.000 tweets para cada búsqueda.

Dado las funcionalidades descritas anteriormente (Descarga de tweets, almacenamiento en base de datos y la nube), esta aplicación está compuesta por tres componentes principales y cada uno con funcionalidades. 

1. Descarga de tweets: `pruebas_jorge/twint_twitter.py`
2. Cargar los datos en la base de datos: `pruebas_jorge/insert_data_database.py`
3. Insertar los datos a la nube de Google Cloud: `pruebas_jorge/insert_data_bucket_gcp.py`

Cada componente tiene diferentes funciones que soportan su operación.

1. Ejecutando el lanzador, `twint_launcher.sh` -Esta configurado por medio de un cronjob para que se ejecute todo los dias a las 00:01:00 UTC, este contiene los tres script principales, el de descarga de los tweets, el de cargar los datos a la base de datos y el de cargar los datos a la nube de Google, estos se ejecutan de forma síncrona.
2. twint_twitter: `twint_twitter.py` - Hace la configuracón inicial del modulo de Twint y configura parámetros como la cantidad de tweets que se descargan por cada búsqueda, el tipo de archivo que se descarga y carga las reglas por las cuales se harán las búsquedas en Tweeter, por último guarda en el servidor un archivo con la información de la búsqueda.
3. insert_data_database: `insert_data_database.py` - Este script carga el modulo psycopg2 que es un controlador de PostgreSQL, ejecuta el modulo config.py el cual llama las credenciales necesarias que están en el servidor y tiene la configuración para conectarse a la base de datos, construye los Querys para hacer la inserción de los datos, configura la ruta donde se encuentran almacenados en el servidor y por último ejecuta los querys para cargar el archivo a la base de datos.
4. insert_data_bucket_gcp: `insert_data_bucket_gcp.py` -Este script carga el modulo storage de google.cloud el cual nos conecta al bucket por medio de un archivo .json que contiene las credenciales necesarias. Configura las rutas donde estan los archivos en el servidor y donde se almacenarán en la nube. Con estos parámetros se hace la carga de los datos desde el servidor hasta el bucket y por último elimina el archivo del dia anterior para liberar espacio de almacenamiento en el servidor.

## Instalación

Para que el prototipo funcione el servidor de despliegue requiere **Python 3.6.2** junto con varias dependencias. Para la instalación de Python hay varios métodos como Anaconda, virtualenvs, entre otros. Nosotros sugerimos usar [Anaconda](https://www.anaconda.com/). Para instalar Anaconda puede seguir la guía de instalación disponible en [Anaconda Install](https://docs.anaconda.com/anaconda/install/)

Una vez Python haya sido instalado y el ambiente de instalación con **Python 3.6.2** activado, debe instalar los requerimientos para que la aplicación funcione. Para esto debe ejecutar, una vez ubicado en el directorio raíz del proyecto:

`pip install -r requirements_pip.txt`


## Reglas

Las reglas actualmente se encuentran en el directorio `/pruebas_jorge` el cual se encuentra en el directorio raíz del servidor, si se cambia de ubicación estas reglas se deberían actualizar las variables ‘regla_es’ y ‘regla_en’ que se encuentran en el script  `twint_twitter.py`.

una regla es un parámetro con uno o varios argumentos y condiciones, estas reglas satisfacen los parámetros de búsqueda de la red social Tweeter.

Ejemplos de reglas con un solo argumento:

- @portafolioco
- GDP
- from:@facebook
- Presidential elections

Ejemplo de reglas con varios argumentos y condiciones:

- (Facebook Contenido) AND (Cuentas Expertas)
- from:@CocaColaCo OR from:@CocaCola
- Coca-Cola OR (coca cola) OR (coke AND -(sniff OR narcotics OR powder)) OR $KO
- from:@bethanymac12 OR rt:@bethanymac12 OR from:@grlemkau OR rt:@grlemkau OR from:@HayekAndKeynes OR rt:@HayekAndKeynes OR from:@LizAnnSonders OR rt:@LizAnnSonders OR from:@russian_market OR rt:@russian_market OR from:@BullandBaird OR rt:@BullandBaird OR from:@JustinWolfers OR rt:@JustinWolfers OR from:@PaulKrugman

## Accediendo a los datos

Dado que la aplicación guarda las descargas en dos bases de datos diferentes, una base de datos relacional y otra en un almacenamiento en la nube podemos acceder a los datos de dos maneras.

1. La base de datos se encuentra en el servidor donde se aloja la aplicación, para ingresar a ella basta con tener las credenciales necesarias y definir qué administrador utilizar para visualizar los datos, también se puede utilizar un controlador como  **psycopg2** de Python para traer los datos y analizarlos.
2. El almacenamiento  en la nube está en **Google Cloud Storage** es un servicio de almacenamiento de archivos en línea RESTful para almacenar y acceder a datos en la infraestructura de **Google cloud Platform**. El servicio combina el rendimiento y la escalabilidad de la nube de Google con capacidades avanzadas de seguridad y uso compartido. Con los permisos y credenciales necesarios se puede descargar los archivos directamente desde la nube a cualquier host local.

Para que la aplicación sea más funcional se diseñaron dos script para descargar los archivos desde la nube hasta cualquier host o servidor con Python, esto puede ser útil si se trabaja desde un servidor sin entorno visual o para automatizar procesos de análisis. los script son:

- `list_of_file_bucket.py`, ejecutando este script podemos acceder al bucket en la nube y ver una lista de los archivos en ella.
- `Descarga_Bucket_GCP.ipynb`, sabiendo que archivos hay en el bucket este script permite descargar uno o varios de los archivos al host o servidor donde se ejecute, para ejecutar cualquiera de los script mencionados se debe contar con las credenciales necesarias.    

## Licencia

Todo el software en este repositorio pertenece a [Quantil S.A.S.](http://quantil.co/) 

