# ------------------------------------------------------------------------------------
# Cargar los archivos descargados de los Tweets por cada dia en Google Cloud Storage
# Elimina el archivo del dia anterior dejando solo el dia actual.
# -----------------------------------------------------------------------------------

### ***** Importar librerias ***** ###
import datetime
from datetime import timedelta
from google.cloud import storage
import os
import sys
import shutil


### ***** Fecha actual y dia anterior ***** ###
d = datetime.date.today()
dt = d - timedelta(days=1)
d=str(d)

### ***** Ruta de archivos que se eliminaran ***** ###
path_es_rm = '/home/jorgeda/pruebas_jorge/BDD/tweets_español/'+str(dt)
path_en_rm = '/home/jorgeda/pruebas_jorge/BDD/tweets_ingles/'+str(dt)

### ***** Ruta de GCS donde se cargaran los archivos ***** ###
path_es1 = "tweets_espaniol/All_Data-tweets_español-"+d+".csv.gz"
path_en1 = "tweets_ingles/All_Data-tweets_ingles-"+d+".csv.gz"

### ***** Ruta de archivos que se cargaran a GCS ***** ###
path_es='/home/jorgeda/pruebas_jorge/BDD/tweets_español/'+d+'/'+'All_Data-tweets_español-'+d+'.csv.gz'
path_en='/home/jorgeda/pruebas_jorge/BDD/tweets_ingles/'+d+'/'+'All_Data-tweets_ingles-'+d+'.csv.gz'


###***** Inserción interactiva en un Bucket de Google Cloud Storage *****###

# Configurar GCS con las credenciales suministrada por la API de GCP
storage_client = storage.Client.from_service_account_json(r'/home/jorgeda/pruebas_jorge/socialdata-psw.json')
# Configurar el Bucket que se usara
bucket = storage_client.get_bucket('s_d_bucket')

# Insertar datos en español al Bucket
blob = bucket.blob(path_es1)
blob.upload_from_filename(path_es)

# Insertar datos en ingles al Bucket
blob = bucket.blob(path_en1)
blob.upload_from_filename(path_en)



# -----------------------------------------------------------------------------------------
# Eliminar el archivo del dia entarior del servidor para que solo quede el actual
# Se hace una excepción para que no falle es script si el archivo no se encuentra
# -----------------------------------------------------------------------------------------
try:
    shutil.rmtree(path_es_rm)
except OSError as e:
    print ("Error: %s - %s." % (e.filename, e.strerror))
    
try:
    shutil.rmtree(path_en_rm)
except OSError as e:
    print ("Error: %s - %s." % (e.filename, e.strerror))